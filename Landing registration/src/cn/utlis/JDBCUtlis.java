package cn.utlis;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.sql.DataSource;

import com.alibaba.druid.pool.DruidDataSourceFactory;
//druid工具类
public class JDBCUtlis {
	private static DataSource ds;
	static {
		Properties pro=new Properties();
		try {
			pro.load(JDBCUtlis.class.getClassLoader().getResourceAsStream("druid.properties"));
			ds=DruidDataSourceFactory.createDataSource(pro);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static DataSource getDataSource() {
		return ds;
	}
	public static Connection getConnection() throws SQLException {
		return ds.getConnection();
	}
}
