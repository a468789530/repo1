package cn.domain;
//获取数据库信息

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import cn.main.User;
import cn.utlis.JDBCUtlis;



public class sql {
	private static JdbcTemplate temp=new JdbcTemplate(JDBCUtlis.getDataSource());

	//登陆
	public User login(User loginuser) {
		try {
			String sql="SELECT * FROM user WHERE username = ? AND password = ?";
			User user = temp.queryForObject(sql, new BeanPropertyRowMapper<User>(User.class),loginuser.getUsername(),loginuser.getPassword());
			return	user;
		}catch (Exception e) {
			return null;
		}
	}	
	
	//注册
	public int register(User user) {
		try {
		String sql="INSERT INTO user (username,PASSWORD,nickname,sex,email,date) VALUES (?,?,?,?,?,?);";
		int update = temp.update(sql,user.getUsername(),user.getPassword(),user.getNickname(),user.getSex(),user.getEmail(),user.getDate());
		return update;
		}catch (Exception e) {
			return 0;
		}
	}		
}
